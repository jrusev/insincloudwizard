import React from 'react';
import { Link } from 'react-router-dom';
import { siteConfig } from '../../settings';
import Icon from '../../image/favicon.png';
import '../../customStyles/main.css';
import SiteLogo from '../../image/site-logo.png';

export default ({ collapsed }) => {
  return (
    <div className="isoLogoWrapper">
      {collapsed ? (
        <div>
          <h3>
            <Link to="/dashboard">
              {/*<i className={siteConfig.siteIcon} />*/}
              <img src = {Icon} className = "DashboardIconCollapsed" />
            </Link>
          </h3>
        </div>
      ) : (
        <h3 className = "DashboardIconLink">
          <Link to="/dashboard" >
            <img src = {SiteLogo} className = "DashboardIcon" />
          </Link>
        </h3>
      )}
    </div>
  );
};
