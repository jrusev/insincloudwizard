import React from 'react';
import ReactDOM from 'react-dom';
import DashApp from './dashApp';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';
import * as serviceWorker from './serviceWorker';
import 'antd/dist/antd.css';
/*import { ApolloProvider } from 'react-apollo'
import { ApolloClient } from 'apollo-client'
import { createHttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'*/

ReactDOM.render(<DashApp />, document.getElementById('root'));

serviceWorker.unregister();