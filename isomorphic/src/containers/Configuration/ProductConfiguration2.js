import React, {Component} from 'react';
import { gql } from "apollo-boost";
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from '@apollo/react-hooks';
import './ConfigurationStyles/ProductConfiguration.css';
import home from '../../image/home-blue.png';
import homeActive from '../../image/home.png'
import car from '../../image/car-blue.png';
import finance from '../../image/finance-blue.png';
import travel from '../../image/travel-blue.png';
import clone from '../../image/clone.png';
import allianz from '../../image/allianz.gif';
import armeec from '../../image/armeec.png';
import bulins from '../../image/bulins.png';
import olympic from '../../image/olympic.png';
import Input, {
    InputGroup,
  } from '../../components/uielements/input';
  import Select, { SelectOption } from '../../components/uielements/select';
  import { Icon, Row, Col, Collapse, Switch } from 'antd';
  import Collapses from '../../components/uielements/collapse';
  import IntlMessages from '../../components/utility/intlMessages';
  import LayoutWrapper from "../../components/utility/layoutWrapper.js";
  import basicStyle from "../../settings/basicStyle";

const text = <IntlMessages id="uiElements.collapse.text" />;
const Panel = Collapses.Panel;

  const Option = SelectOption;

const client = new ApolloClient({
    uri: 'http://127.0.0.1:8000/graphiql/',
});

class ProductConfiguration extends Component {
    constructor(props){
        super(props);

        this.state = {
            Products: [
                {
                    id: 'sasax',
                    name: 'Property Insurance',
                    counter: 1,
                    iconURL: home,
                    activeIcon: homeActive,
                },
                {
                    id: 'cccc',
                    name: 'Car Insurance',
                    counter: 2,
                    iconURL: car,
                },
                {
                    id: 'sdcs',
                    name: 'Financial Insurance',
                    counter: 5,
                    iconURL: finance,
                },
                {
                    id: 'aaa',
                    name: 'Travel Insurance',
                    counter: 7,
                    iconURL: travel,
                },
            ],
            checked: false,
            doesShow: false,
        };
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount(){
        this.refreshList();
    }

    refreshList = () => {
        client
          .query({
              query:gql
                  `{products {
                    id
                    productName
                    productDescription
                    fields {
                      id
                      fieldName
                      fieldType
                      groups {
                        id
                        groupName
                      }
                    }
                  }}`
          })
      .then(result => console.log(result));
        };

        handleChange(checked) {
            this.setState({ checked });
          }

        toggleCompanies = () => {
            const doesShow = this.state.doesShow;
            this.setState({doesShow: !doesShow});
        }

    render () {
        const { rowStyle, colStyle, gutter } = basicStyle;
        const headerAllianz = (
            <div className="CompanyHeader">
                <Switch checked/>
                <span className="CompanyLogo">
                    <img src={allianz}/>
                </span>
                <span className="CompanyName" style={{marginRight: '133px'}}>Allianz</span>
                <span className="ActiveInsurance">3 active insurances</span>
                <div className='Controls'>
                    <span><img src={clone} alt="clone"/></span>
                    <span><a href="#" style={{textDecoration: 'none', color: 'gray'}}><Icon type="edit" className="isoNoteEditIcon" /></a></span>
                </div>
            </div>
        )
        const headerArmeec = (
            <div className="CompanyHeader">
                <Switch />
                <span className="CompanyLogo">
                    <img src={armeec}/>
                </span>
                <span className="CompanyName" style={{marginRight: '125px'}}>Armeec</span>
                <span className="ActiveInsurance">2 active insurances</span>
                <div className='Controls'>
                    <span><img src={clone} alt="clone"/></span>
                    <span><a href="http://localhost:3000/dashboard/productcorrection" style={{textDecoration: 'none', color: 'gray'}}><Icon type="edit" className="isoNoteEditIcon" /></a></span>
                </div>
            </div>
        )
        const headerBulins = (
            <div className="CompanyHeader">
                <Switch checked/>
                <span className="CompanyLogo">
                    <img src={bulins}/>
                </span>
                <span className="CompanyName" style={{marginRight: '137px'}}>Bulins</span>
                <span className="ActiveInsurance">3 active insurances</span>
                <div className='Controls'>
                    <span><img src={clone} alt="clone"/></span>
                    <span><a href="#" style={{textDecoration: 'none', color: 'gray'}}><Icon type="edit" className="isoNoteEditIcon" /></a></span>
                </div>
            </div>
        )
        const headerOlympic = (
            <div className="CompanyHeader">
                <Switch />
                <span className="CompanyLogo">
                    <img src={olympic}/>
                </span>
                <span className="CompanyName" style={{marginRight: '123px'}}>Olympic</span>
                <span className="ActiveInsurance" style={{marginRight: '360px'}}>1 active insurance</span>
                <div className='Controls'>
                    <span><img src={clone} alt="clone"/></span>
                    <span><a href="#" style={{textDecoration: 'none', color: 'gray'}}><Icon type="edit" className="isoNoteEditIcon" /></a></span>
                </div>
            </div>
        )
        return <LayoutWrapper className = 'ddd'>
                <Row style={rowStyle} gutter={gutter} justify="start">
                    <Col onClick={this.toggleCompanies} className={this.state.doesShow ? 'ProductCardActive' : 'ProductCard'}>
                        <p className={this.state.doesShow ? 'CounterActive' : 'Counter'}>{this.state.Products[0].counter}</p>
                        <p className={this.state.doesShow ? 'ProductNameActive' : 'ProductName'}>{this.state.Products[0].name}</p>
                        <div className="IconHolder" style={this.state.doesShow ? {backgroundImage: `url(${this.state.Products[0].activeIcon})`}:{backgroundImage: `url(${this.state.Products[0].iconURL})`}}></div>
                    </Col>
                    <Col className='ProductCard'>
                        <p className='Counter'>{this.state.Products[1].counter}</p>
                        <p className='ProductName'>{this.state.Products[1].name}</p>
                        <div className="IconHolder" style={{backgroundImage: `url(${this.state.Products[1].iconURL})`}}></div>
                    </Col>
                    <Col className='ProductCard'>
                        <p className='Counter'>{this.state.Products[2].counter}</p>
                        <p className='ProductName'>{this.state.Products[2].name}</p>
                        <div className="IconHolder" style={{backgroundImage: `url(${this.state.Products[2].iconURL})`}}></div>
                    </Col>
                    <Col className='ProductCard'>
                        <p className='Counter'>{this.state.Products[3].counter}</p>
                        <p className='ProductName'>{this.state.Products[3].name}</p>
                        <div className="IconHolder" style={{backgroundImage: `url(${this.state.Products[3].iconURL})`}}></div>
                    </Col>
                </Row>

                <InputGroup compact style={{ marginBottom: '15px' }} className="CustomInput">
                    <Select defaultValue="Option1" style={{ width: '33%' }}>
                        <Option value="Option1">Insurance 1</Option>
                        <Option value="Option2">Insurance 2</Option>
                        <Option value="Option3">Insurance 3</Option>
                        <Option value="Option4">Insurance 4</Option>
                        <Option value="Option5">Insurance 5</Option>
                        <Option value="Option6">Insurance 6</Option>
                    </Select>
                </InputGroup>
                {
                    this.state.doesShow ? <div className='Companies'>
                    <h5>Companies</h5>
                    <Collapse bordered={false} className="CustomCollapse">
                        <Panel header={headerAllianz} key="1" className="CustomPanel">
                            <div style={{background: '#ededed'}} className="CompanyItem">
                                <Switch/>
                                <span className="MaxCoverage">Allianz-maximum coverage</span>
                                <span className="TestVersion">Test version</span>
                                <span className="Api">API</span>
                                <div className='Controls'>
                                    <span><img src={clone} alt="clone" /></span>
                                    <span><Icon type="edit" className="isoNoteEditIcon" /></span>
                                </div>
                            </div>
                            <div style={{background: '#ffffff'}} className="CompanyItem">
                                <Switch checked/>
                                <span className="MaxCoverage">Allianz-maximum coverage</span>
                                <span className="LiveVersion">Live version</span>
                                <span className="Api">API</span>
                                <div className='Controls'>
                                    <span><img src={clone} alt="clone" /></span>
                                    <span><Icon type="edit" className="isoNoteEditIcon" /></span>
                                </div>
                            </div>
                            <div style={{background: '#ededed'}} className="CompanyItem">
                                <Switch />
                                <span className="ExpandedCoverage">Allianz-expanded coverage</span>
                                <span className="TestVersion">Test version</span>
                                <span className="PricingHub">Pricing Hub</span>
                                <div className='Controls'>
                                    <span><img src={clone} alt="clone" /></span>
                                    <span><Icon type="edit" className="isoNoteEditIcon" /></span>
                                </div>
                            </div>
                            <div style={{background: '#ffffff'}} className="CompanyItem">
                                <Switch checked/>
                                <span className="ExpandedCoverage">Allianz-expanded coverage</span>
                                <span className="LiveVersion">Live version</span>
                                <span className="Api">API</span>
                                <div className='Controls'>
                                    <span><img src={clone} alt="clone" /></span>
                                    <span><Icon type="edit" className="isoNoteEditIcon" /></span>
                                </div>
                            </div>
                            <div style={{background: '#ededed'}} className="CompanyItem">
                                <Switch />
                                <span className="HouseCoverage">Allianz-house coverage</span>
                                <span className="TestVersion">Test version</span>
                                <span className="PricingHub">Pricing Hub</span>
                                <div className='Controls'>
                                    <span><img src={clone} alt="clone" /></span>
                                    <span><Icon type="edit" className="isoNoteEditIcon" /></span>
                                </div>
                            </div>
                            <div style={{background: '#ffffff'}} className="CompanyItem">
                                <Switch checked/>
                                <span className="HouseCoverage">Allianz-house coverage</span>
                                <span className="LiveVersion">Live version</span>
                                <span className="Api">API</span>
                                <div className='Controls'>
                                    <span><img src={clone} alt="clone" /></span>
                                    <span><Icon type="edit" className="isoNoteEditIcon" /></span>
                                </div>
                            </div>
                        </Panel>
                        <Panel header={headerArmeec} key="2" className="CustomPanel">
                            <div style={{background: '#ededed'}} className="CompanyItem">
                                <Switch/>
                                <span className="MaxCoverage" style={{marginRight: '220px'}}>Armeec-maximum coverage</span>
                                <span className="TestVersion">Test version</span>
                                <span className="PricingHub">Pricing Hub</span>
                                <div className='Controls'>
                                    <span><img src={clone} alt="clone" /></span>
                                    <span><Icon type="edit" className="isoNoteEditIcon" /></span>
                                </div>
                            </div>
                            <div style={{background: '#ffffff'}} className="CompanyItem">
                                <Switch checked/>
                                <span className="MaxCoverage" style={{marginRight: '220px'}}>Armeec-maximum coverage</span>
                                <span className="LiveVersion">Live version</span>
                                <span className="Api">API</span>
                                <div className='Controls'>
                                    <span><img src={clone} alt="clone" /></span>
                                    <span><Icon type="edit" className="isoNoteEditIcon" /></span>
                                </div>
                            </div>
                            <div style={{background: '#ededed'}} className="CompanyItem">
                                <Switch />
                                <span className="ExpandedCoverage">Allianz-expanded coverage</span>
                                <span className="TestVersion">Test version</span>
                                <span className="PricingHub">Pricing Hub</span>
                                <div className='Controls'>
                                    <span><img src={clone} alt="clone" /></span>
                                    <span><Icon type="edit" className="isoNoteEditIcon" /></span>
                                </div>
                            </div>
                            <div style={{background: '#ffffff'}} className="CompanyItem">
                                <Switch checked/>
                                <span className="ExpandedCoverage">Allianz-expanded coverage</span>
                                <span className="LiveVersion">Live version</span>
                                <span className="Api">API</span>
                                <div className='Controls'>
                                    <span><img src={clone} alt="clone" /></span>
                                    <span><Icon type="edit" className="isoNoteEditIcon" /></span>
                                </div>
                            </div>
                        </Panel>
                        <Panel header={headerBulins} key="3" className="CustomPanel">
                            <div style={{background: '#ededed'}} className="CompanyItem">
                                <Switch/>
                                <span className="MaxCoverage">Bulins-maximum coverage</span>
                                <span className="TestVersion">Test version</span>
                                <span className="PricingHub">Pricing Hub</span>
                                <div className='Controls'>
                                    <span><img src={clone} alt="clone" /></span>
                                    <span><Icon type="edit" className="isoNoteEditIcon" /></span>
                                </div>
                            </div>
                            <div style={{background: '#ffffff'}} className="CompanyItem">
                                <Switch checked/>
                                <span className="MaxCoverage">Bulins-maximum coverage</span>
                                <span className="LiveVersion">Live version</span>
                                <span className="Api">API</span>
                                <div className='Controls'>
                                    <span><img src={clone} alt="clone" /></span>
                                    <span><Icon type="edit" className="isoNoteEditIcon" /></span>
                                </div>
                            </div>
                            <div style={{background: '#ededed'}} className="CompanyItem">
                                <Switch />
                                <span className="ExpandedCoverage">Bulins-expanded coverage</span>
                                <span className="TestVersion">Test version</span>
                                <span className="PricingHub">Pricing Hub</span>
                                <div className='Controls'>
                                    <span><img src={clone} alt="clone" /></span>
                                    <span><Icon type="edit" className="isoNoteEditIcon" /></span>
                                </div>
                            </div>
                            <div style={{background: '#ffffff'}} className="CompanyItem">
                                <Switch checked/>
                                <span className="ExpandedCoverage">Bulins-expanded coverage</span>
                                <span className="LiveVersion">Live version</span>
                                <span className="Api">API</span>
                                <div className='Controls'>
                                    <span><img src={clone} alt="clone" /></span>
                                    <span><Icon type="edit" className="isoNoteEditIcon" /></span>
                                </div>
                            </div>
                            <div style={{background: '#ededed'}} className="CompanyItem">
                                <Switch />
                                <span className="HouseCoverage">Bulins-house coverage</span>
                                <span className="TestVersion">Test version</span>
                                <span className="Api">API</span>
                                <div className='Controls'>
                                    <span><img src={clone} alt="clone" /></span>
                                    <span><Icon type="edit" className="isoNoteEditIcon" /></span>
                                </div>
                            </div>
                            <div style={{background: '#ffffff'}} className="CompanyItem">
                                <Switch checked/>
                                <span className="HouseCoverage">Bulins-house coverage</span>
                                <span className="LiveVersion">Live version</span>
                                <span className="PricingHub">Pricing Hub</span>
                                <div className='Controls'>
                                    <span><img src={clone} alt="clone" /></span>
                                    <span><Icon type="edit" className="isoNoteEditIcon" /></span>
                                </div>
                            </div>
                        </Panel>
                        <Panel header={headerOlympic} key="4" className="CustomPanel">
                            <div style={{background: '#ededed'}} className="CompanyItem">
                                <Switch/>
                                <span className="MaxCoverage">Olympic-maximum coverage</span>
                                <span className="TestVersion">Test version</span>
                                <span className="PricingHub" style={{marginRight: '131px'}}>Pricing Hub</span>
                                <div className='Controls'>
                                    <span><img src={clone} alt="clone" /></span>
                                    <span><Icon type="edit" className="isoNoteEditIcon" /></span>
                                </div>
                            </div>
                            <div style={{background: '#ffffff'}} className="CompanyItem">
                                <Switch checked/>
                                <span className="MaxCoverage">Olympic-maximum coverage</span>
                                <span className="LiveVersion">Live version</span>
                                <span className="Api">API</span>
                                <div className='Controls'>
                                    <span><img src={clone} alt="clone" /></span>
                                    <span><Icon type="edit" className="isoNoteEditIcon" /></span>
                                </div>
                            </div>
                        </Panel>
                    </Collapse>
                </div> : null
                }
                
            </LayoutWrapper>
    }
}

export default ProductConfiguration;