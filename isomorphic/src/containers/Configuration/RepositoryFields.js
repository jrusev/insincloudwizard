import React, { Component } from 'react';
import { Row, Col, Collapse, Input, Button, Modal, Form, Select, Checkbox } from 'antd';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import './ConfigurationStyles/RepositoryFields.css';
import filter from '../../image/filter.png';

const formItemLayout = {
    labelCol: { span: 4 },
    wrapperCol: { span: 8 },
};
const formTailLayout = {
    labelCol: { span: 4 },
    wrapperCol: { span: 8, offset: 4 },
};
const { Option } = Select;
const { Panel } = Collapse;
const { Search } = Input;

function callback(num) {
    console.log(num);
  }
// fake data generator
const getItems = (count, offset = 0) =>
    Array.from({ length: count }, (v, k) => k).map(k => ({
        id: `item-${k + offset}`,
        content: `item ${k + offset}`
    }));


// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
};

/**
 * Moves an item from one list to another list.
 */
const move = (source, destination, droppableSource, droppableDestination) => {
    const sourceClone = Array.from(source);
    const destClone = Array.from(destination);
    const [removed] = sourceClone.splice(droppableSource.index, 1);

    destClone.splice(droppableDestination.index, 0, removed);

    const result = {};
    result[droppableSource.droppableId] = sourceClone;
    result[droppableDestination.droppableId] = destClone;

    return result;
};

const grid = 8;

const getItemStyle = (isDragging, draggableStyle) => ({
    // some basic styles to make the items look a bit nicer
    userSelect: 'none',
    margin: `0 8px ${grid}px 8px`,
    width: '240px',
    height: '36px',

    // change background colour if dragging
    background: isDragging ? 'lightgreen' : 'grey',

    // styles we need to apply on draggables
    ...draggableStyle
});

const getListStyle = isDraggingOver => ({
    background: isDraggingOver ? 'lightblue' : 'white',
    padding: grid,
    width: '100%',
    display: 'flex',
    flexWrap: 'wrap'
});

class RepositoryFields extends Component {
    constructor(props){
        super(props);
        this.state = {
            items: getItems(14),
            selected: getItems(0),
            modalVisible: false,
            checkNick: false,
            isActive: 5,
        };
    }

    setModalVisible(modalVisible) {
        this.setState({ modalVisible });
    }

    check = () => {
        this.props.form.validateFields(err => {
            if (!err) {
                console.info('success');
            }
        });
    };

    handleChange = e => {
        this.setState(
            {
                checkNick: e.target.checked,
            },
            () => {
                this.props.form.validateFields(['nickname'], { force: true });
            },
        );
    };

    /**
     * A semi-generic way to handle multiple lists. Matches
     * the IDs of the droppable container to the names of the
     * source arrays stored in the state.
     */
    id2List = {
        droppable: 'items',
        droppable2: 'selected'
    };

    getList = id => this.state[this.id2List[id]];

    moveOnClick = (source, destination) => {
        if (!destination) {
            return;
        }

        if (source.droppableId === destination.droppableId) {
            const items = reorder(
                this.getList(source.droppableId),
                source.index,
                destination.index
            );

            let state = { items };

            if (source.droppableId === 'droppable2') {
                state = { selected: items };
            }

            this.setState(state);
        } else {
            const result = move(
                this.getList(source.droppableId),
                this.getList(destination.droppableId),
                source,
                destination
            );

            this.setState({
                items: result.droppable,
                selected: result.droppable2
            });
        }
    };

    onDragEnd = result => {
        const { source, destination } = result;
        // dropped outside the list
        if (!destination) {
            return;
        }

        if (source.droppableId === destination.droppableId) {
            const items = reorder(
                this.getList(source.droppableId),
                source.index,
                destination.index
            );

            let state = { items };

            if (source.droppableId === 'droppable2') {
                state = { selected: items };
            }

            this.setState(state);
        } else {
            const result = move(
                this.getList(source.droppableId),
                this.getList(destination.droppableId),
                source,
                destination
            );

            this.setState({
                items: result.droppable,
                selected: result.droppable2
            });
        }
    };

    render() {
        const { formLayout } = this.state;
        const { getFieldDecorator } = this.props.form;
        const buttonItemLayout =
            formLayout === 'horizontal'
                ? {
                    wrapperCol: { span: 14, offset: 4 },
                }
                : null;

        const droppableContent = (provided, snapshot) => (
            <div
                ref={provided.innerRef}
                style={getListStyle(snapshot.isDraggingOver)}>
                {this.state.selected.map((item, index) => (
                    <Draggable
                        key={item.id}
                        draggableId={item.id}
                        index={index}>
                        {(provided, snapshot) => (
                            <div
                                ref={provided.innerRef}
                                {...provided.draggableProps}
                                {...provided.dragHandleProps}
                                style={getItemStyle(
                                    snapshot.isDragging,
                                    provided.draggableProps.style
                                )}>
                                {item.content}
                            </div>
                        )}
                    </Draggable>
                ))}
                {provided.placeholder}
            </div>
        )

        return (
            <DragDropContext onDragEnd={this.onDragEnd}>
                <Row type="flex" className="rf-Wrapper">
                    <Col className="rf-Repository">
                        <div className="rf-Repository--head">
                            <span>Repository Fields</span>
                            <img src={filter} alt="filter" style={{marginRight: '10px'}}/>
                        </div>
                        <Search
                            placeholder="Refine your search"
                            onSearch={value => console.log(value)}
                            style={{ width: '100%' }}
                        />
                        <Droppable droppableId="droppable">
                            {(provided, snapshot) => (
                                <div
                                    ref={provided.innerRef}
                                    style={getListStyle(snapshot.isDraggingOver)}>
                                    {this.state.items.map((item, index) => (
                                        <Draggable
                                            key={item.id}
                                            draggableId={item.id}
                                            index={index}>
                                            {(provided, snapshot) => (
                                                <div onClick={() => this.moveOnClick(
                                                    {index: index, droppableId: "droppable"},
                                                    {droppableId: "droppable2", index: 0}
                                                )}
                                                    ref={provided.innerRef}
                                                    {...provided.draggableProps}
                                                    {...provided.dragHandleProps}
                                                    style={getItemStyle(
                                                        snapshot.isDragging,
                                                        provided.draggableProps.style
                                                    )}>
                                                    {item.content}
                                                </div>
                                            )}
                                        </Draggable>
                                    ))}
                                    {provided.placeholder}
                                </div>
                            )}
                        </Droppable>
                        <Button onClick={()=>this.setModalVisible(true)}>Add New Field</Button>
                        <Modal
                            title="Add New Repository Fields"
                            centered
                            visible={this.state.modalVisible}
                            onOk={() => this.setModalVisible(false)}
                            onCancel={() => this.setModalVisible(false)}
                            width="488px"
                        >
                            <Form>
                                <Form.Item {...formItemLayout}>
                                    {getFieldDecorator('username', {
                                        rules: [
                                            {
                                                required: false,
                                                message: 'Please input your name',
                                            },
                                        ],
                                    })(<Input placeholder="Name" />)}
                                </Form.Item>
                                <div className="AddNewProductTypeLabel">
                                    <span>Choose field type</span>
                                </div>
                                <Form.Item>
                                    {getFieldDecorator('select2', {
                                        rules: [{ required: false, message: 'Please select a field type!' }],
                                    })(
                                        <Select placeholder="Types" style={{width: '100%'}}>
                                            <Option value="type1">Type1</Option>
                                            <Option value="type2">Type2</Option>
                                        </Select>,
                                    )}
                                </Form.Item>
                                <div className="AddNewProductTypeLabel">
                                    <span>Choose group</span>
                                </div>
                                <Form.Item>
                                    {getFieldDecorator('select', {
                                        rules: [{ required: false, message: 'Please select a group!' }],
                                    })(
                                        <Select 
                                            placeholder="Groups" 
                                            style={{width: '100%'}}>
                                            <Option value="group1">Group1</Option>
                                            <Option value="group2">Group2</Option>
                                        </Select>,
                                    )}
                                </Form.Item>
                            </Form>
                        </Modal>
                    </Col>
                    <Col className="rf-DataContainer">
                        <Collapse /*defaultActiveKey={['1']}*/ onChange={(event)=>{this.setState({isActive: event})}} accordion={true} expandIconPosition={['right']}>
                            <Panel header="This is panel header 1" key="3">
                                <Droppable droppableId="dfvdfvdfv">
                                    {this.state.isActive == 3 ? droppableContent : null}
                                </Droppable>
                            </Panel>
                            <Panel header="This is panel header 2" key="4">
                                <Droppable droppableId="dfvvvvvvvvv">
                                    {(provided, snapshot) => (
                                        <div
                                            ref={provided.innerRef}
                                            style={getListStyle(snapshot.isDraggingOver)}>
                                            {this.state.selected.map((item, index) => (
                                                <Draggable
                                                    key={item.id}
                                                    draggableId={item.id}
                                                    index={index}>
                                                    {(provided, snapshot) => (
                                                        <div
                                                            ref={provided.innerRef}
                                                            {...provided.draggableProps}
                                                            {...provided.dragHandleProps}
                                                            style={getItemStyle(
                                                                snapshot.isDragging,
                                                                provided.draggableProps.style
                                                            )}>
                                                            {item.content}
                                                        </div>
                                                    )}
                                                </Draggable>
                                            ))}
                                            {provided.placeholder}
                                        </div>
                                    )}
                                </Droppable>
                            </Panel>
                        </Collapse>
                    </Col>
                </Row>
            </DragDropContext>
        );
    }
}

const WrappedRepositoryFields = Form.create({ name: 'repository_fields' })(RepositoryFields);
export default WrappedRepositoryFields;