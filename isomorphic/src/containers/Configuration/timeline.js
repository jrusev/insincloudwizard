import React, {Component} from 'react';
import { Steps, message, Row, Col, Icon } from 'antd';
import PageHeader from '../../components/utility/pageHeader';
import Box from '../../components/utility/box';
import LayoutWrapper from '../../components/utility/layoutWrapper.js';
import ContentHolder from '../../components/utility/contentHolder';
import basicStyle from '../../settings/basicStyle';
import CollapseWrapper from '../Uielements/Collapse/collapse.style';
import './ConfigurationStyles/timeline.css';
import Select, { SelectOption } from '../../components/uielements/select';
import Button, { ButtonGroup } from '../../components/uielements/button';
import IntlMessages from '../../components/utility/intlMessages';
import { rtl } from '../../settings/withDirection';
import AddNewProduct from './AddNewProduct';
import RepositoryFields from './RepositoryFields';
import CompanyInformation from './CompanyInformation';
import ProductCorrection from './ProductCorrection';
import gql from 'graphql-tag';

const { Step } = Steps;

const steps = [
  {
    title: 'First',
    content: <AddNewProduct />,
  },
  {
    title: 'Second',
    content: <RepositoryFields/>,
  },
  {
    title: 'Third',
    content: <CompanyInformation/>,
  },
  {
    title: 'Fourth',
    content: <ProductCorrection/>,
  },  
];


class Timeline extends Component {

	constructor(props) {
    super(props);
    this.state = {
      current: 0,
    };
  }

  next() {
    const current = this.state.current + 1;
    this.setState({ current });
  }

  prev() {
    const current = this.state.current - 1;
    this.setState({ current });
  }

  state = {
    current: 0,
  };

  onChange = current => {
    console.log('onChange:', current);
    this.setState({ current });
  };

  render() {
    const { current } = this.state;
    const { rowStyle, colStyle, gutter } = basicStyle;
    return (
      <div>
      		<div className="timeline">
				<Row style={rowStyle} gutter={gutter} justify="start">
					<Col md={4} sm={24} xs={24} style={colStyle} >
						<div className="timelineButtons">	
							<Button block ghost style={{marginTop: '15px', color: 'white'}}>
								Preview
							</Button>
							<Button block ghost style={{marginTop: '15px', color: 'white'}}>
								Live Version <Icon type="setting" />
							</Button>
						</div>
					</Col>
					<Col md={16} sm={24} xs={24} style={colStyle}>
						<Steps current={current} onChange={this.onChange}>
						  <Step title="Insurance Information" description="This is a description." />
						  <Step title="Product Information" description="This is a description." />
						  <Step title="Company Information" description="This is a description." />
						  <Step title="Insurance Product" description="This is a description." />
						</Steps>
					</Col>
					<Col md={4} sm={24} xs={24} style={colStyle}>
					</Col>
				</Row>
			</div>
        <div className="steps-content">{steps[current].content}</div>
        <div className="steps-action">
          {current < steps.length - 1 && (
            <Button type="primary" onClick={() => this.next()}>
              Next
            </Button>
          )}
          {current === steps.length - 1 && (
            <Button type="primary" onClick={() => message.success('Processing complete!')}>
              Done
            </Button>
          )}
          {current > 0 && (
            <Button style={{ marginLeft: 8 }} onClick={() => this.prev()}>
              Previous
            </Button>
          )}
        </div>
      </div>
    );
  }

}

export default Timeline;