import React, {Component} from 'react';
import {Icon, Row, Col, Upload, message, Input, Checkbox, Rate } from 'antd';
import Collapses from '../../components/uielements/collapse';
/*import PageHeader from '../../components/utility/pageHeader';*/
import Box from '../../components/utility/box';
import LayoutWrapper from '../../components/utility/layoutWrapper.js';
/*import ContentHolder from '../../components/utility/contentHolder';*/
import basicStyle from '../../settings/basicStyle';
import CollapseWrapper from '../Uielements/Collapse/collapse.style';
import './ConfigurationStyles/CompanyInformation.css';
import './ConfigurationStyles/ProductConfiguration.css';
import Select, { SelectOption } from '../../components/uielements/select';
import Button, { ButtonGroup } from '../../components/uielements/button';
/*import IntlMessages from '../../components/utility/intlMessages';*/
/*import { rtl } from '../../settings/withDirection';*/
const Option = SelectOption;

const { Search } = Input;

const { Dragger } = Upload;

const { TextArea } = Input;

/*const props = {
  name: 'file',
  multiple: true,
  action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
  onChange(info) {
    const { status } = info.file;
    if (status !== 'uploading') {
      console.log(info.file, info.fileList);
    }
    if (status === 'done') {
      message.success(`${info.file.name} file uploaded successfully.`);
    } else if (status === 'error') {
     message.error(`${info.file.name} file upload failed.`);
    }
  },
};
*/
const children = [];

for (let i = 10; i < 36; i++) {
  children.push(<Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>);
}
const Panel = Collapses.Panel;
const Collapse = props => (
  <CollapseWrapper>
    <Collapses {...props}>{props.children}</Collapses>
  </CollapseWrapper>
);
/*const ColoredLine = ({ color }) => (
    <hr
        style={{
            color: color,
            backgroundColor: color,
            height: 1
        }}
    />
);*/

function onChange(checkedValues) {
  console.log('checked = ', checkedValues);
}

const plainOptions = ['Бургас', 'София', 'Сливен', 'Велико Търново', 'Добрич', 'Стара Загора', 'Габрово'];

class CompanyInformation extends Component {
  callback = key => {};
  render() {
    const { rowStyle, colStyle, gutter } = basicStyle;
    return (
        <LayoutWrapper>
            <Box>
                <LayoutWrapper>
                    <Row style={rowStyle} gutter={gutter} justify="start">
                        <Col md={8} sm={8} xs={24} style={colStyle}>
                            <div className="filterTitles">
                              <Icon type="profile" className="filterIcons" theme="filled" />
                              <p>Name</p>
                            </div>
                            <Select
                              defaultValue="Name"
                              onChange={this.handleChange}
                              style={{ width: '100%' }}
                            >
                              <Option value="correction">Name 1</Option>
                              <Option value="surname">Name 2</Option>
                              <Option value="correction3">Name 3</Option>
                            </Select>
                        </Col>
                        <Col offset={1} md={8} sm={8} xs={24} style={colStyle} >
                            <div className="filterTitles">
                              <Icon type="picture" theme="filled" className="filterIcons"/>
                              <p>Logo</p>
                            </div>
                            <Dragger style={{height: '50px'}} >
                              <p className="ant-upload-text">Drop file here or upload</p>
                            </Dragger>
                        </Col>
                    </Row>
                    <Row style={rowStyle} gutter={gutter} justify="start">
                        <Col md={5} sm={5} xs={15} style={colStyle}>
                            <div className="filterTitles">
                                <Icon type="percentage" className="filterIcons"/>
                                <p>Company Discount</p>
                            </div>
                            <Select
                              defaultValue="All products"
                              onChange={this.handleChange}
                              style={{ width: '100%' }}
                            >
                                <Option value="correction">Name 1</Option>
                                <Option value="surname">Name 2</Option>
                                <Option value="correction3">Name 3</Option>
                            </Select>
                        </Col>
                        <Col offset={1} md={2} sm={2} xs={8} style={colStyle}>
                            <div className="filterTitles">
                                <p>&nbsp;</p>
                            </div>
                            <Select
                              defaultValue="Choose"
                              onChange={this.handleChange}
                              style={{ width: '100%' }}
                            >
                                <Option value="correction">Name 1</Option>
                                <Option value="surname">Name 2</Option>
                                <Option value="correction3">Name 3</Option>
                          </Select>
                        </Col>
                        <Col offset={1} md={8} sm={8} xs={24} style={colStyle} >
                            <div className="filterTitles">
                              <p>Description</p>
                            </div>
                            <TextArea rows={6} placeholder="Write a description of the product" style={{resize: 'none'}}/>
                        </Col>
                    </Row>
                </LayoutWrapper>           
            </Box>
                        
            <Row style={rowStyle} gutter={gutter} justify="start">
              <Col md={24} sm={24} xs={24} style={colStyle}>
                <Collapse accordion>
                  <Panel
                    header=<p className="conditionHeader" > Group Field by Company </p>
                  >
                    <Row style={rowStyle} gutter={gutter} justify="start">
                      <Col md={11} sm={11} xs={24} style={colStyle}>
                        <div className="conditionFilter">
                          <p>Select Groups</p>
                          <Select
                            defaultValue="Name"
                            onChange={this.handleChange}
                            style={{ width: '100%' }}
                          >
                            <Option value="correction">Name 1</Option>
                            <Option value="surname">Name 2</Option>
                            <Option value="correction3">Name 3</Option>
                          </Select>
                        </div>
                      </Col> 
                    </Row>
                    <Collapse accordion>
                      <Panel
                        header=<p className="informationHeader" > Name of the selected group </p>
                      > 
                        <Row style={rowStyle} gutter={gutter} justify="start">
                          <Col md={6} sm={12} xs={24} style={colStyle}>
                            <div class="searchBox">
                              <Search
                                placeholder="Refine your search"
                                onSearch={value => console.log(value)}
                                style={{ width: '100%', marginBottom: '16px' }}
                              />
                              <div>
                                <Checkbox.Group options={plainOptions} defaultValue={['Бургас', 'Добрич']} onChange={onChange} style={{display: 'grid', lineHeight: '2'}} />
                              </div>
                            </div>
                          </Col> 
                          <Col md={6} sm={12} xs={24} style={colStyle}>
                            <div class="searchBox">
                              <Input placeholder="Name of the group" style={{ width: '100%', marginBottom: '16px' }} />
                              <div>
                                <Checkbox.Group options={plainOptions} defaultValue={['Стара Загора', 'Сливен']} onChange={onChange} style={{display: 'grid', lineHeight: '2'}} />
                              </div>
                            </div>
                          </Col> 
                          <Col md={6} sm={12} xs={24} style={colStyle}>
                            <div class="searchBox">
                              <Input placeholder="Name of the group" style={{ width: '100%', marginBottom: '16px' }} />
                              <div>
                                <Checkbox.Group options={plainOptions} defaultValue={['Габрово', 'Добрич']} onChange={onChange} style={{display: 'grid', lineHeight: '2'}} />
                              </div>
                            </div>
                          </Col> 
                          <Col md={6} sm={12} xs={24} style={colStyle}>
                            <div class="searchBox">
                              <Button style={{marginTop: '104px', marginBottom: '104px'}}block>Add New Group</Button>
                            </div>
                          </Col> 
                        </Row>
                      </Panel>
                    </Collapse>
                  </Panel>
                </Collapse>
              </Col> 
            </Row>

            <Row style={rowStyle} gutter={gutter} justify="start">
              <Col md={24} sm={24} xs={24} style={colStyle}>
                <Collapse accordion>
                  <Panel
                    header=<p className="conditionHeader" > Ratings </p>
                  >
                    <Row style={rowStyle} gutter={gutter} justify="start">
                      <Col md={11} sm={11} xs={24} style={colStyle}>
                        <div className="conditionFilter">
                          <p>New Rating</p>
                          <Select
                            defaultValue="Name"
                            onChange={this.handleChange}
                            style={{ width: '100%' }}
                          >
                            <Option value="correction">Name 1</Option>
                            <Option value="surname">Name 2</Option>
                            <Option value="correction3">Name 3</Option>
                          </Select>
                        </div>
                      </Col> 
                    </Row>

                    <Row style={rowStyle} gutter={gutter} justify="start">
                      <Col md={6} sm={12} xs={24} style={colStyle}>
                        <p className="ratingHeader">Рейтинг</p>
                        <Rate />
                      </Col> 
                      <Col md={6} sm={12} xs={24} style={colStyle}>
                        <p className="ratingHeader">Рейтинг</p>
                        <Rate />
                      </Col>
                      <Col md={6} sm={12} xs={24} style={colStyle}>
                        <p className="ratingHeader">Рейтинг</p>
                        <Rate />
                      </Col>
                      <Col md={6} sm={12} xs={24} style={colStyle}>
                        <p className="ratingHeader">Рейтинг</p>
                        <Rate />
                      </Col>
                    </Row>
                  </Panel>
                </Collapse>
              </Col> 
            </Row>



                        <Row style={rowStyle} gutter={gutter} justify="start">
                          <Col md={24} sm={24} xs={24} style={colStyle}>
                            <Button type="primary" style={{float: 'right'}}>
                              Save
                            </Button>
                          </Col> 
                        </Row>
        </LayoutWrapper>
    );
  }
}

export default CompanyInformation;