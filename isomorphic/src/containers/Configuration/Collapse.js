import React, {Component} from 'react';
import {Icon, Row, Col } from 'antd';
import Collapses from '../../components/uielements/collapse';
/*import PageHeader from '../../components/utility/pageHeader';*/
import Box from '../../components/utility/box';
/*import LayoutWrapper from '../../components/utility/layoutWrapper.js';*/
/*import ContentHolder from '../../components/utility/contentHolder';*/
import basicStyle from '../../settings/basicStyle';
import CollapseWrapper from '../Uielements/Collapse/collapse.style';
import './ConfigurationStyles/ProductConfiguration.css';
import Select, { SelectOption } from '../../components/uielements/select';
import Button, { ButtonGroup } from '../../components/uielements/button';
/*import IntlMessages from '../../components/utility/intlMessages';
import { rtl } from '../../settings/withDirection';*/
const Option = SelectOption;

const children = [];
for (let i = 10; i < 36; i++) {
  children.push(<Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>);
}
const Panel = Collapses.Panel;
const Collapse = props => (
  <CollapseWrapper>
    <Collapses {...props}>{props.children}</Collapses>
  </CollapseWrapper>
);
const ColoredLine = ({ color }) => (
    <hr
        style={{
            color: color,
            backgroundColor: color,
            height: 1
        }}
    />
);

class CollapsePanel extends Component {
    callback = key => {};
        render() {
            const { rowStyle, colStyle, gutter } = basicStyle;
            return (
                <Collapse accordion>
                  <Panel
                    header=<p className="panelHeader" ><Icon type="appstore" className="tabIcon"/> Correction 1 </p>
                    key="1"
                  >
                      <Col md={6} sm={6} xs={24} style={colStyle}>
                        <Box>
                          <div className="filterTitles">
                            <Icon className="filterIcons" type="edit" />
                            <p>Correction</p>
                          </div>
                          <Select
                            defaultValue="Name of the correction"
                            onChange={this.handleChange}
                            style={{ width: '100%' }}
                          >
                            <Option value="correction">Correction 1</Option>
                            <Option value="surname">Correction 2</Option>
                            <Option value="correction3">Correction 3</Option>
                          </Select>
                        </Box>
                      </Col>
                      <Col md={6} sm={6} xs={24} style={colStyle}>
                        <Box>
                          <div className="filterTitles">
                            <Icon className="filterIcons" type="calculator" />
                            <p>Function</p>
                          </div>
                          <Select
                            defaultValue="Choose function"
                            onChange={this.handleChange}
                            style={{ width: '100%' }}
                          >
                            <Option value="correction">Correction 1</Option>
                            <Option value="surname">Correction 2</Option>
                            <Option value="correction3">Correction 3</Option>
                          </Select>
                        </Box>
                      </Col>
                      <Col md={8} sm={8} xs={24} style={colStyle}>
                        <Box>
                          <div className="filterTitles">
                            <Icon className="filterIcons" type="percentage" />
                            <p>Value</p>
                          </div>
                          <Select
                            defaultValue="Indicate value"
                            onChange={this.handleChange}
                            style={{ width: '100%' }}
                          >
                            <Option value="correction">Correction 1</Option>
                            <Option value="surname">Correction 2</Option>
                            <Option value="correction3">Correction 3</Option>
                          </Select>
                        </Box>
                      </Col>
                      <Col md={4} sm={4} xs={24} style={colStyle}>
                        <Box>
                          <div className="filterTitles">
                            <p>&nbsp;</p>
                          </div>
                          <Select
                            defaultValue="Indicate value"
                            onChange={this.handleChange}
                            style={{ width: '100%' }}
                          >
                            <Option value="correction">Correction 1</Option>
                            <Option value="surname">Correction 2</Option>
                            <Option value="correction3">Correction 3</Option>
                          </Select>
                        </Box>
                      </Col>
                      <Col md={12} sm={12} xs={24} style={colStyle}>
                        <Box>
                          <div className="filterTitles">
                            <Icon className="filterIcons" type="flag" />
                            <p>Type of the correction</p>
                          </div>
                          <Select
                            defaultValue="Choose"
                            onChange={this.handleChange}
                            style={{ width: '100%' }}
                          >
                            <Option value="correction">Correction 1</Option>
                            <Option value="surname">Correction 2</Option>
                            <Option value="correction3">Correction 3</Option>
                          </Select>
                        </Box>
                      </Col>
                      <Row style={rowStyle} gutter={gutter} justify="start">
                        <Col md={24} sm={24} xs={24} style={colStyle}>
                          <Button type="primary" style={{float: 'right'}}>
                            Save
                          </Button>
                        </Col> 
                      </Row>
                      <Row style={rowStyle} gutter={gutter} justify="start">
                        <Col md={24} sm={24} xs={24} style={colStyle}>
                          <ColoredLine color="#ededed" />
                        </Col> 
                      </Row>
                      <Row style={rowStyle} gutter={gutter} justify="start">
                        <Col md={24} sm={24} xs={24} style={colStyle}>
                          <Collapse accordion>
                            <Panel
                              header=<p className="conditionHeader" > Condition </p>
                            >
                              <Row style={rowStyle} gutter={gutter} justify="start">
                                <Col md={11} sm={11} xs={24} style={colStyle}>
                                  <div className="conditionFilter">
                                    <p>Filter Conditions</p>
                                    <Icon type="filter" style={{ fontSize: 22, color: 'grey', padding: 15 }} />
                                      <Box>
                                        <Select
                                          defaultValue="Group Operations"
                                          onChange={this.handleChange}
                                          style={{ width: '100%' }}
                                        >
                                          <Option value="correction">Correction 1</Option>
                                          <Option value="surname">Correction 2</Option>
                                          <Option value="correction3">Correction 3</Option>
                                        </Select>
                                      </Box>
                                  </div>
                                </Col> 
                              </Row>
                              <Row style={rowStyle} gutter={gutter} justify="start">
                                <Col md={12} sm={12} xs={24} style={colStyle}>
                                  <div className="inputfield">
                                    <div className="inputfieldHeader">
                                      <p className="conditionHeader"> Condition 1 </p>
                                      <div className="conditionIcons">
                                        <Icon type="edit" />
                                        <Icon type="copy" />
                                        <Icon type="delete" />
                                      </div>  
                                    </div>
                                    <div className="inputfieldContent">
                                      <p>First Payment</p>
                                      <p>80.00 bgn</p>
                                    </div>
                                    <div className="inputfieldContent">
                                      <p>Second Payment</p>
                                      <p>20.00 bgn</p>
                                    </div>
                                    <div className="inputfieldContent">
                                      <p>Third Payment</p>
                                      <p>90.00 bgn</p>
                                    </div>
                                    <div className="inputfieldContent">
                                      <p>Value of the property</p>
                                      <p>20.00 bgn</p>
                                    </div>
                                    <div className="inputfieldContent">
                                      <p>Tax</p>
                                      <p>20.00 bgn</p>
                                    </div>
                                    <div className="inputfieldContent">
                                      <p>Pre-payment</p>
                                      <p>30.00 bgn</p>
                                    </div>
                                    <div className="inputfieldContent">
                                      <p>Pre-payment</p>
                                      <p>30.00 bgn</p>
                                    </div>
                                  </div>
                                </Col> 
                                <Col md={12} sm={12} xs={24} style={colStyle}>
                                  <div className="inputfield">
                                    <div className="inputfieldHeader">
                                      <p className="conditionHeader"> Condition 1 </p>
                                      <div className="conditionIcons">
                                        <Icon type="edit" />
                                        <Icon type="copy" />
                                        <Icon type="delete" />
                                      </div>  
                                    </div>
                                    <div className="inputfieldContent">
                                      <p>First Payment</p>
                                      <p>80.00 bgn</p>
                                    </div>
                                    <div className="inputfieldContent">
                                      <p>Second Payment</p>
                                      <p>20.00 bgn</p>
                                    </div>
                                    <div className="inputfieldContent">
                                      <p>Third Payment</p>
                                      <p>90.00 bgn</p>
                                    </div>
                                    <div className="inputfieldContent">
                                      <p>Value of the property</p>
                                      <p>20.00 bgn</p>
                                    </div>
                                    <div className="inputfieldContent">
                                      <p>Tax</p>
                                      <p>20.00 bgn</p>
                                    </div>
                                    <div className="inputfieldContent">
                                      <p>Pre-payment</p>
                                      <p>30.00 bgn</p>
                                    </div>
                                    <div className="inputfieldContent">
                                      <p>Pre-payment</p>
                                      <p>30.00 bgn</p>
                                    </div>
                                  </div>
                                </Col> 
                              </Row>
                            </Panel>
                          </Collapse>
                        </Col> 
                      </Row>
                  </Panel>
                </Collapse>
            );
        }
}

export default CollapsePanel;