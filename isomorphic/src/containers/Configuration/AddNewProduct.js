import React, {Component} from 'react';
import { Icon, Form, Input, Button, Checkbox, Row, Col } from 'antd';
import './ConfigurationStyles/test.css';
import ApolloClient, { gql } from 'apollo-boost';
import Select from 'react-select';
import home from '../../static/Images/home.svg';

const client = new ApolloClient({
    uri: 'http://127.0.0.1:8000/graphiql/',
    onError: ({ networkError, graphQLErrors }) => {
        console.log('graphQLErrors', graphQLErrors)
        console.log('networkError', networkError)
      },
});

/*const options = [
    { value: 'icon1', label: <span><img src={home}/> icon1</span> },
    { value: 'icon2', label: 'icon2' },
    { value: 'icon3', label: 'icon3' },
  ];*/

const options = [];

const formItemLayout = {
    labelCol: { span: 4 },
    wrapperCol: { span: 8 },
};
const formTailLayout = {
    labelCol: { span: 4 },
    wrapperCol: { span: 8, offset: 4 },
};
const { Option } = Select;
const { TextArea } = Input;

class AddNewProduct extends Component {
    constructor(props){
        super(props);

        this.state = {
            checkNick: false,
            name: "",
            description: "",
            selectedOption: null,
        }
    }

    check = () => {
        this.props.form.validateFields(err => {
            if (!err) {
                console.info('success');
            }
        });
    };

    handleChange = e => {
        this.setState(
            {
                checkNick: e.target.checked,
            },
            () => {
                this.props.form.validateFields(['nickname'], { force: true });
            },
        );
    };

    handleSelectChange = selectedOption => {
        this.setState(
          { selectedOption }
        );
      };

      componentDidMount(){
        this.refreshList();
    }

    refreshList = () => {
        client
          .query({
              query:gql
                  `{
                    icons {
                      iconName
                    }
                  }`
          })
      .then(result => {
          for(var i = 0; i < result['data']['icons'].length; i++ ){
            options.push(
                {
                    value: result['data']['icons'][i]['iconName'],
                    label: <span><img src={require('../../static/Images/home.svg')}/> {result['data']['icons'][i]['iconName']}</span>,
                }
            )
          }
      });
        };

    createNewProduct = () => {
        client.mutate({
            mutation:gql`
              mutation {
                createProduct(productData: 
                  {
                    productName: "${this.state.name}",
                    productDescription: "dscsdcsdc",
                    productType: "Type2",
                  }){
                  product {
                    productName
                    productDescription
                    productType
                    fields {
                      id
                    }
                  }
                }
              }
            `
        })
    }

    render() {
        const { selectedOption } = this.state;
        const { formLayout } = this.state;
        const { getFieldDecorator } = this.props.form;
        const buttonItemLayout =
            formLayout === 'horizontal'
                ? {
                    wrapperCol: { span: 14, offset: 4 },
                }
                : null;
        return (
            <Form>
                <div className="AddProductBoxWrapper">
                    <div className="AddProductBox">
                        <div className="AddProductBoxHeader">
                            <h1>Insurance Information</h1>
                            <Icon type="info-circle" theme="filled" style={{color: '#459fed', marginLeft: '12px'}}/>
                        </div>
                        <Row type="flex" className="AddProductInputsHolder">
                            <Col className="AddNewProductNameCol">
                                <div className="AddNewProductNameLabel">Name</div>
                                <Form.Item {...formItemLayout}>
                                    {getFieldDecorator('username', {
                                        initialValue: "aaa",
                                        rules: [
                                            {
                                                required: false,
                                                message: 'Please input your name',
                                            },
                                        ],
                                    })(<Input placeholder="Name" onChange={e => this.setState({name: e.target.value})}/>)}
                                </Form.Item>
                            </Col>
                            <Col className="ChooseProductTypeCol">
                                <div className="AddNewProductTypeLabel">
                                    <span>Product Type</span>
                                    <span style={{fontSize: '13px', color: '#505050'}}>+Add new product type</span>
                                </div>
                                <Form.Item hasFeedback>
                                    {getFieldDecorator('select', {
                                        initialValue: "Type1",
                                        rules: [{ required: false, message: 'Please select a product type!' }],
                                    })(
                                        <Select
                                            //value={selectedOption}
                                            onChange={this.handleSelectChange}
                                            //options={options}
                                        />
                                    )}
                                </Form.Item>
                            </Col>
                            <Col className="ChooseIconCol">
                                <div className="AddNewProductIconLabel">Choose Icon</div>
                                <Form.Item hasFeedback>
                                    {getFieldDecorator('select2', {
                                        initialValue: "Icon1",
                                        rules: [{ required: false, message: 'Please select an icon!' }],
                                    })(
                                        <Select
                                            value={selectedOption}
                                            onChange={this.handleSelectChange}
                                            options={options}
                                        />
                                    )}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row className="AddNewProductDescriptionRow">
                            <Col className="AddNewProductDescription">
                                <div className="AddNewProductDescriptionLabel">Description</div>
                                <Form.Item {...formItemLayout}>
                                    {getFieldDecorator('Textarea', {
                                        initialValue: "dscsdcsdcsdc",
                                        rules: [
                                            {
                                                required: false,
                                                message: 'Please input your name',
                                            },
                                        ],
                                    })(<TextArea 
                                            rows={6} 
                                            placeholder="Write a description of the product" 
                                            style={{resize: 'none'}}/>)}
                                </Form.Item>
                            </Col>
                        </Row>
                    </div>
                    <Form.Item {...buttonItemLayout} className="GoNextItem">
                        <Button type="primary" className="GoNext" onClick={this.createNewProduct}>Continue to Product Information</Button>
                    </Form.Item>
                </div>
            </Form>
        )
    }
}
const WrappedAddNewProduct = Form.create({ name: 'add_new_product' })(AddNewProduct);
export default WrappedAddNewProduct