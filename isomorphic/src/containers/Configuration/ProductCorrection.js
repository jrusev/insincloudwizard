import React, {Component} from 'react';
import {Icon, Row, Col } from 'antd';
import Collapses from '../../components/uielements/collapse';
import PageHeader from '../../components/utility/pageHeader';
import Box from '../../components/utility/box';
import LayoutWrapper from '../../components/utility/layoutWrapper.js';
import ContentHolder from '../../components/utility/contentHolder';
import basicStyle from '../../settings/basicStyle';
import CollapseWrapper from '../Uielements/Collapse/collapse.style';
import './ConfigurationStyles/ProductConfiguration.css';
import Select, { SelectOption } from '../../components/uielements/select';
import Button, { ButtonGroup } from '../../components/uielements/button';
import IntlMessages from '../../components/utility/intlMessages';
import { rtl } from '../../settings/withDirection';
import CollapsePanel from './Collapse';
const Option = SelectOption;

const children = [];
for (let i = 10; i < 36; i++) {
  children.push(<Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>);
}
const Panel = Collapses.Panel;
const Collapse = props => (
  <CollapseWrapper>
    <Collapses {...props}>{props.children}</Collapses>
  </CollapseWrapper>
);
const ColoredLine = ({ color }) => (
    <hr
        style={{
            color: color,
            backgroundColor: color,
            height: 1
        }}
    />
);
class ProductCorrection extends Component {
  callback = key => {};
  render() {
    const { rowStyle, colStyle, gutter } = basicStyle;
    return (
      <LayoutWrapper>
        <Row style={rowStyle} gutter={gutter} justify="start">
          <Col md={24} sm={24} xs={24} style={colStyle}>
            <CollapsePanel/> 
            <CollapsePanel/> 
          </Col>
        </Row>
      </LayoutWrapper>
    );
  }
}

export default ProductCorrection;