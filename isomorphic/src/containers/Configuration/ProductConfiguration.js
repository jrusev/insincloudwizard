import React, {Component} from 'react';
import { gql } from "apollo-boost";
import ApolloClient from 'apollo-boost';
/*import { ApolloProvider } from '@apollo/react-hooks';*/
import './ConfigurationStyles/ProductConfiguration.css';
import './ConfigurationStyles/test.css'
import home from '../../image/home-blue.png';
/*import homeActive from '../../image/home.png';*/
import car from '../../image/car-blue.png';
import finance from '../../image/finance-blue.png';
import travel from '../../image/travel-blue.png';
/*import clone from '../../image/clone.png';*/
import allianz from '../../image/allianz.gif';
import armeec from '../../image/armeec.png';
import bulins from '../../image/bulins.png';
/*import olympic from '../../image/olympic.png';*/
import Input, {
    InputGroup,
  } from '../../components/uielements/input';
  import Select, { SelectOption } from '../../components/uielements/select';
  import { Row, Col, Collapse} from 'antd';
  import Collapses from '../../components/uielements/collapse';
  import IntlMessages from '../../components/utility/intlMessages';
  import LayoutWrapper from "../../components/utility/layoutWrapper.js";
  import basicStyle from "../../settings/basicStyle";

/*const text = <IntlMessages id="uiElements.collapse.text" />;*/
const Panel = Collapses.Panel;

  const Option = SelectOption;

const client = new ApolloClient({
    uri: 'http://127.0.0.1:8000/graphiql/',
});

let comp = null;

class ProductConfiguration extends Component {
    constructor(props){
        super(props);

        this.state = {
            Products: [
                {
                  id: 234234,
                  productName: 'Property Insurances',
                  counter: 1,
                  productIcon: home,
                  companies: [
                    {
                      id: 8978,
                      companyName: 'Allianz',
                      companyLogo: allianz,
                    },
                    {
                      id: 7979,
                      companyName: 'Armeec',
                      companyLogo: armeec,
                    },
                    {
                      id: 6978,
                      companyName: 'Bulins',
                      companyLogo: bulins,
                    }
                  ]
                },
                {
                  id: 134434,
                  productName: 'Car Insurance',
                  counter: 2,
                  productIcon: car,
                  companies: [
                    {
                      id: 5978,
                      companyName: 'Allianz',
                      companyLogo: allianz,
                    },
                    {
                      id: 4978,
                      companyName: 'Armeec',
                      companyLogo: armeec,
                    },
                    {
                      id: 3978,
                      companyName: 'Bulins',
                      companyLogo: bulins,
                    }
                  ]
                },
                {
                  id: 734234,
                  productName: 'Financial Insurance',
                  counter: 5,
                  productIcon: finance,
                  companies: [
                    {
                      id: 2978,
                      companyName: 'Allianz',
                      companyLogo: allianz,
                    },
                    {
                      id: 1978,
                      companyName: 'Armeec',
                      companyLogo: armeec,
                    },
                    {
                      id: 897834,
                      companyName: 'Bulins',
                      companyLogo: bulins,
                    }
                  ]
                },
                {
                  id: 934234,
                  productName: 'Travel Insurance',
                  counter: 7,
                  productIcon: travel,
                  companies: [
                    {
                      id: 8978009,
                      companyName: 'Allianz',
                      companyLogo: allianz,
                    },
                    {
                      id: 89785512,
                      companyName: 'Armeec',
                      companyLogo: armeec,
                    },
                    {
                      id: 897864488,
                      companyName: 'Bulins',
                      companyLogo: bulins,
                    }
                  ]
                },
            ],
            checked: false,
            doesShow: false,
        };
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount(){
        this.refreshList();
    }

    refreshList = () => {
        client
          .query({
              query:gql
                  `{products {
                    id
                    productName
                    productDescription
                    fields {
                      id
                      fieldName
                      fieldType
                      groups {
                        id
                        groupName
                      }
                    }
                  }}`
          })
      .then(result => console.log(result));
        };

        handleChange(checked) {
            this.setState({ checked });
          }

        toggleCompanies = (product) => {
            const doesShow = this.state.doesShow;
            this.setState({doesShow: !doesShow});

            comp = product.companies.map((company, companyIndex) => 
            <Panel header={company.companyName} key={company.id} className="CustomPanel">

            </Panel>);

            console.log(comp);
        }

    render () {
        const { gutter } = basicStyle;
        
        return (
            <LayoutWrapper className="ConfigPageWrapper">
                <Row type="flex" className="ProductCardsContainer">
                  {
                    this.state.Products.map((product, index) => 
                      {if(index === 0){
                        return <div key="dcsdc">
                          <Col className="ProductCard" onClick={() => this.toggleCompanies.bind(product)}>
                            <div className="ProductCardUpperRow">
                              {product.counter}
                              <img src={product.productIcon} alt="icon" className="ProductIcon" />
                            </div>
                            <div>
                              {product.productName}
                            </div>
                          </Col>
                          <InputGroup compact style={{ marginBottom: '15px' }}>
                            <Select defaultValue="Option1" className="CustomInput">
                              <Option value="Option1">Insurance 1</Option>
                              <Option value="Option2">Insurance 2</Option>
                              <Option value="Option3">Insurance 3</Option>
                              <Option value="Option4">Insurance 4</Option>
                              <Option value="Option5">Insurance 5</Option>
                              <Option value="Option6">Insurance 6</Option>
                            </Select>
                          </InputGroup>
                        </div>
                      }else{
                        return <Col className="ProductCard" key={product.id}>
                          <div className="ProductCardUpperRow">
                            {product.counter}
                            <img src={product.productIcon} alt="icon" className="ProductIcon" />
                          </div>
                          <div>
                            {product.productName}
                          </div>
                        </Col>
                      }}
                    )
                  }
                </Row>
                {
                  this.state.doesShow ? <div className='Companies'>
                    <Collapse bordered={false} className="CustomCollapse">
                      {
                        comp
                      }
                    </Collapse>
                  </div> : null
                }
            </LayoutWrapper>
        )
    }
}

export default ProductConfiguration;