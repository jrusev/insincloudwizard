import React, {Component} from 'react';
import axios from "axios";
import { ApolloProvider } from '@apollo/react-hooks';
import { useQuery } from '@apollo/react-hooks';

import { gql } from "apollo-boost";
import ApolloClient from 'apollo-boost';

const client = new ApolloClient({
    uri: 'http://127.0.0.1:8000/graphiql/',
  });

class Products extends Component {
    constructor(props){
        super(props);

        this.state = {
            Products: []
        }
    }

    componentDidMount(){
        this.refreshList();
    }

    refreshList = () => {
      client
        .query({
            query:gql
                `{product(id: 2) {
                    id
                    productName
                    productDescription
                    fields {
                      id
                      fieldName
                      fieldType
                      groups {
                        id
                        groupName
                      }
                    }
                  }
                }`
        })
    .then(result => console.log(result));
  	};
    render (){
        {if(this.state.Products.length > 0) {
            //console.log(this.state.Products[0].fields[0]);
        }}
        return (
            <div>
                <ApolloProvider client={client}>
                    <div>
                        <h2>My first Apollo app 🚀</h2>
                    </div>
                </ApolloProvider>
            </div>
        )
    }
}

export default Products;