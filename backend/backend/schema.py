import graphene
from graphene_django.types import DjangoObjectType, ObjectType
from wizzardapp.models import Product, Field, Group, Company, Price

class ProductType(DjangoObjectType):
    class Meta:
        model = Product

class FieldType(DjangoObjectType):
    class Meta:
        model = Field

class GroupType(DjangoObjectType):
    class Meta:
        model = Group

class CompanyType(DjangoObjectType):
    class Meta:
        model = Company

class PriceType(DjangoObjectType):
    class Meta:
        model = Price

class Query(ObjectType):
    product = graphene.Field(ProductType, id = graphene.Int())
    price = graphene.Field(PriceType, id = graphene.Int())
    field = graphene.Field(FieldType, id = graphene.Int())
    products = graphene.List(ProductType)
    fields = graphene.List(FieldType)
    groups = graphene.Field(GroupType, id = graphene.Int())
    group = graphene.List(GroupType)
    companies = graphene.Field(CompanyType, id = graphene.Int())
    company = graphene.List(CompanyType)
    icons = graphene.String();

    def resolve_product(self, info, **kwargs):
        id = kwargs.get('id')

        if id is not None:
            return Product.objects.get(pk = id)

        return None

    def resolve_price(self, info, **kwargs):
        id = kwargs.get('id')

        if id is not None:
            return Price.objects.get(pk = id)

        return None

    def resolve_field(self, info, **kwargs):
        id = kwargs.get('id')

        if id is not None:
            return Field.objects.get(pk = id)

        return None

    def resolve_group(self, info, **kwargs):
        id = kwargs.get('id')

        if id is not None:
            return Group.objects.get(pk = id)

        return None

    def resolve_company(self, info, **kwargs):
        id = kwargs.get('id')

        if id is not None:
            return Company.objects.get(pk = id)

        return None
    

    def resolve_products(self, info, **kwargs):
        return Product.objects.all()

    def resolve_prices(self, info, **kwargs):
        return Price.objects.all()

    def resolve_fields(self, info, **kwargs):
        return Field.objects.all()

    def resolve_groups(self, info, **kwargs):
        return Group.objects.all()

    def resolve_companies(self, info, **kwargs):
        return Company.objects.all()

class FieldInput(graphene.InputObjectType):
    id = graphene.ID()
    field_name = graphene.String()
    field_type = graphene.String()

class ProductInput(graphene.InputObjectType):
    id = graphene.ID()
    product_name = graphene.String()
    product_description = graphene.String()
    product_type = graphene.String()
    product_icon = graphene.String()

class PriceInput(graphene.InputObjectType):
    id = graphene.ID()
    price_name = graphene.String()
    base_price = graphene.String()
    function_name = graphene.String()

class GroupInput(graphene.InputObjectType):
    id = graphene.ID()
    group_name = graphene.String()

class CompanyInput(graphene.InputObjectType):
    id = graphene.ID()
    company_name = graphene.String()
    company_info = graphene.String()
    company_logo = graphene.String()
    company_rate = graphene.String()

class CreateProduct(graphene.Mutation):
    product = graphene.Field(ProductType)

    class Arguments:
        product_data = ProductInput(required=True)

    @staticmethod
    def mutate(root, info, product_data):
        product = Product.objects.create(**product_data)
        return CreateProduct(product=product)

class CreatePrice(graphene.Mutation):
    price = graphene.Field(PriceType)

    class Arguments:
        price_data = PriceInput(required=True)

    @staticmethod
    def mutate(root, info, price_data):
        if int(price_data.base_price) >= 10:
            price_data.base_price = str(int(price_data.base_price) / 2)
        else:
            price_data.base_price = str(int(price_data.base_price) * 2)
        price = Price(base_price=price_data.base_price, function_name=price_data.function_name)
        price.save()
        return CreatePrice(price=price)

class Mutation(graphene.ObjectType):
    create_product = CreateProduct.Field()
    create_price = CreatePrice.Field()

schema = graphene.Schema(query = Query, mutation=Mutation)  