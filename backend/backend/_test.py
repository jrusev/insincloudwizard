import graphene
from itertools import chain
from graphene_django.types import DjangoObjectType
from graphene_django.fields import DjangoConnectionField

from wizzardapp import models

class ProductNode(DjangoObjectType):
    class Meta:
        model = models.Product
        interfaces = (graphene.Node, )

class FieldNode(DjangoObjectType):
    class Meta:
        model = models.Field
        interfaces = (graphene.Node, )

class Query(graphene.ObjectType):
    product = graphene.Node.Field(ProductNode)
    all_products = DjangoConnectionField(ProductNode)

    field = graphene.Node.Field(FieldNode)
    all_fields = DjangoConnectionField(FieldNode)

    #result_list = list(chain(all_products, all_fields))

    def resolve_product(self, info, **kwargs):
        return ProductNode()

    def resolve_field(self, info, **kwargs):
        return FieldNode()

schema = graphene.Schema(query=Query)