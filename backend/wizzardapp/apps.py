from django.apps import AppConfig


class WizzardappConfig(AppConfig):
    name = 'wizzardapp'
