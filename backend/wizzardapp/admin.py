from django.contrib import admin
from .models import Product, Field, Group, Functions, Condition, CrossSale, Filter, Promo, Company, Price
from django_admin_listfilter_dropdown.filters import (
    DropdownFilter, ChoiceDropdownFilter, RelatedDropdownFilter
)

class FilterAdmin(admin.ModelAdmin):
    ...
    list_filter = (
        # for ordinary fields
        ('a_charfield', DropdownFilter),
        # for choice fields
        ('a_choicefield', ChoiceDropdownFilter),
        # for related fields
        ('a_foreignkey_field', RelatedDropdownFilter),
    )

class ProductAdmin(admin.ModelAdmin):
	list_display = ('product_name', 'product_description', 'fields')

class FieldAdmin(admin.ModelAdmin):
	list_display = ('field_name', 'field_type', 'groups')

class CompanyAdmin(admin.ModelAdmin):
    list_display = ('company_name', 'groups', 'companies_logo', 'companies_rate')

class FunctionsAdmin(admin.ModelAdmin):
	list_display = ('function_name', 'groups', 'function_type', 'calculation_type')

class ConditionAdmin(admin.ModelAdmin):
	list_display = ('condition_name', 'groups', 'condition_function')

class CrossSaleAdmin(admin.ModelAdmin):
	list_display = ('crosssale_name', 'crosssale_text', 'crosssale_price', 'crosssale_image')

class PromoAdmin(admin.ModelAdmin):
	list_display = ('promo_name', 'groups', 'promo_function')

class PriceAdmin(admin.ModelAdmin):
    list_display = ('price_name','base_price', 'function_name')

# Register your models here.
admin.site.register(Product, ProductAdmin)
admin.site.register(Field, FieldAdmin)
admin.site.register(Company, CompanyAdmin)
admin.site.register(Group),
admin.site.register(Functions, FunctionsAdmin)
admin.site.register(Condition, ConditionAdmin)
admin.site.register(CrossSale, CrossSaleAdmin)
admin.site.register(Filter, FilterAdmin)
admin.site.register(Promo, PromoAdmin)
admin.site.register(Price, PriceAdmin)
