from django.db import models
from django.shortcuts import render_to_response

TYPE_CHOICES = (
	('boolean', 'BOOLEAN'),
	('text', 'TEXT'),
)

RATE_CHOICES = (
	('low', 'LOW'),
	('poor', 'POOR'),
	('average', 'AVERAGE'),
	('good', 'GOOD'),
	('awesome', 'AWESOME'),
)

FUNCTION_TYPE = (
	('funcion1', 'funcion1'),
	('funcion2', 'funcion2'),
	('funcion3', 'funcion3'),
	('funcion4', 'funcion4'),
)

CALCULATION_TYPE = (
	('calculation1', 'calculation1'),
	('calculation2', 'calculation2'),
	('calculation3', 'calculation3'),
	('calculation4', 'calculation4'),
)
CONDITION_FUNCTION = (
	('condition1', 'condition1'),
	('condition2', 'condition2'),
	('condition3', 'condition3'),
	('condition4', 'condition4'),
)
PROMO_FUNCTION = (
	('promo1', 'promo1'),
	('promo2', 'promo2'),
	('promo3', 'promo3'),
	('promo4', 'promo4'),
)
# Create your models here.

class Product(models.Model):
	PRODUCT_ICONS = (
		('icon1', 'icon1'),
		('icon2', 'icon2'),
		('icon3', 'icon3'),
	)
	product_name = models.CharField(max_length = 50)
	product_description = models.TextField()
	product_type = models.CharField(max_length = 120)
	product_icon = models.CharField(max_length=20, choices=PRODUCT_ICONS, blank=True, null=True)
	product_price = models.DecimalField(max_digits = 5, decimal_places = 2, blank = True, null = True)
	fields = models.ManyToManyField('Field', related_name = 'fields')
	company = models.ManyToManyField('Company', related_name = 'company')

	def __str__(self):
		return self.product_name

class Field(models.Model):
	field_name = models.CharField(max_length = 50)
	field_type = models.CharField(max_length = 20, choices = TYPE_CHOICES)
	groups = models.ForeignKey('Group', on_delete = models.CASCADE, blank = True, null = True)

	def __str__(self):
		return self.field_name

class CompanyProduct(models.Model):
	from_company = models.ForeignKey('Company', on_delete = models.CASCADE, blank = True, null = True)
	cp_name = models.CharField(max_length=120)

class Company(models.Model):
	company_name = models.CharField(max_length = 50)
	companies_info = models.TextField(max_length=140, default='info')
	companies_logo = models.ImageField(upload_to = "static/img/", null = False, blank = False, default='logo')
	companies_rate = models.CharField(max_length = 20, choices = RATE_CHOICES)
	groups = models.ForeignKey('Group', on_delete = models.CASCADE, blank = True, null = True)

	def __str__(self):
		return self.company_name

	def clone(self, existing_id, clone_name):
		comp = Company.objects.get(pk = existing_id)
		comp.pk = None
		comp.company_name = clone_name
		comp.save()
 
class Group(models.Model):
	group_name = models.CharField(max_length = 100, blank = True)

	def __str__(self):
		return self.group_name

class Functions(models.Model):
	function_name = models.CharField(max_length = 50)
	groups = models.ForeignKey('Group', on_delete = models.CASCADE)
	function_type = models.CharField(max_length = 20, choices = FUNCTION_TYPE)
	calculation_type = models.CharField(max_length = 20, choices = CALCULATION_TYPE)

	def __str__(self):
		return self.function_name

class Condition(models.Model):
	condition_name = models.CharField(max_length = 50)
	groups = models.ForeignKey('Group', on_delete = models.CASCADE)
	condition_function = models.CharField(max_length = 20, choices = CONDITION_FUNCTION)

	def __str__(self):
		return self.condition_name

class CrossSale(models.Model):
	crosssale_name = models.CharField(max_length = 50)
	crosssale_text = models.TextField()
	crosssale_price = models.DecimalField(max_digits = 5, decimal_places = 2, blank = True, null = True)
	crosssale_image = models.ImageField(upload_to = "static/img/", null = True, blank = True)

	def __str__(self):
		return self.crosssale_name

class Filter(models.Model):
	crosssale_name = models.CharField(max_length = 50)
	a_charfield = models.TextField()
	a_choicefield = models.TextField()
	a_foreignkey_field = models.ForeignKey('Group', on_delete = models.CASCADE, blank = True, null = True)

	def __str__(self):
		return self.crosssale_name

class Promo(models.Model):
	promo_name = models.CharField(max_length = 50)
	groups = models.ForeignKey('Group', on_delete = models.CASCADE)
	promo_function = models.CharField(max_length = 20, choices = PROMO_FUNCTION)

	def __str__(self):
		return self.promo_name

class Price(models.Model):
	price_name = models.CharField(max_length = 50)
	base_price = models.CharField(max_length = 20)
	function_name = models.CharField(max_length = 20, choices = PROMO_FUNCTION)

	def __str__(self):
		return self.price_name