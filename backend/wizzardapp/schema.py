import graphene
import backend.wizzardapp.schema

class Query(backend.wizzardapp.schema.Query, graphene.ObjectType):
    pass

class Mutation(backend.wizzardapp.schema.Mutation, graphene.ObjectType):
    pass

schema = graphene.Schema(query = Query, mutation = Mutation)