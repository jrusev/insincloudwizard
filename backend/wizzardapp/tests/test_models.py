# File: ./backend/simple_app/tests/test_models.py

import pytest
from mixer.backend.django import mixer

# We need to do this so that writing to the DB is possible in our tests.
pytestmark = pytest.mark.django_db


def test_message():
    obj = mixer.blend('wizzardapp.Message')
    assert obj.pk > 0

def test_product():
    obj = mixer.blend('wizzardapp.Product')
    assert obj.pk > 0

'''def test_field():
    obj = mixer.blend('wizzardapp.Field')
    assert obj.pk > 0'''