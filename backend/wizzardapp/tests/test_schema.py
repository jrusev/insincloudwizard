import pytest
from mixer.backend.django import mixer

from . import schema


pytestmark = pytest.mark.django_db


def test_message_type():
	instance = schema.MessageType()
	assert instance


def test_resolve_all_messages():
	mixer.blend('wizzardapp.Message')
	mixer.blend('wizzardapp.Message')
	q = schema.Query()
	res = q.resolve_all_messages(None, None, None)
	assert res.count() == 2, 'Should return all messages'

def test_product_type():
	instance = schema.ProductType()
	assert instance


def test_resolve_all_products():
	mixer.blend('wizzardapp.Product')
	mixer.blend('wizzardapp.Product')
	q = schema.Query()
	res = q.resolve_all_products(None, None, None)
	assert res.count() == 2, 'Should return all Products'

'''def test_field_type():
	instance = schema.FieldType()
	assert instance


def test_resolve_all_fields():
	mixer.blend('wizzardapp.Field')
	mixer.blend('wizzardapp.Field')
	q = schema.Query()
	res = q.resolve_all_fields(None, None, None)
	assert res.count() == 2, 'Should return all Fields'
	'''